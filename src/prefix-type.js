export default (prefix, fn, delimiter = '/') => (type, ...args) => fn(prefix + delimiter + type, ...args)
