import prefixType from './prefix-type'

describe('prefixType()', () => {
  it('returns a function', () => {
    expect(typeof prefixType()).toBe('function')
  })
  it('prefixes only the first parameter', () => {
    const curried = prefixType('test', (a, b) => ({ a, b }))
    expect(curried('unprefixed', 5)).toEqual({ a: 'test/unprefixed', b: 5 })
  })
  it('allows to override the delimiter', () => {
    const curried = prefixType('test', (a, b) => ({ a, b }), '////')
    expect(curried('unprefixed', 5)).toEqual({ a: 'test////unprefixed', b: 5 })
  })
})
