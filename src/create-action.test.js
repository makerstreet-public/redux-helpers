import createAction from './create-action'

describe('createAction()', () => {
  let actionCreator = createAction()
  it('createAction returns a function', () => {
    expect(typeof actionCreator).toBe('function')
  })
  it('actionCreator returns an object', () => {
    expect(typeof actionCreator()).toBe('object')
  })
  it('actionCreator returns an object with type undefined', () => {
    expect(actionCreator()).toEqual({ type: undefined })
  })
})
describe('createAction("test")', () => {
  let actionCreator = createAction('test')

  it('returns an actionCreator with toString() => "test"', () => {
    expect(actionCreator.toString()).toEqual('test')
  })
  it('actionCreator returns an object with type: "test"', () => {
    expect(actionCreator()).toEqual({ type: 'test' })
  })
  it('actionCreator(input) returns an object with payload: input', () => {
    let input = 'test2'
    expect(actionCreator(input)).toEqual({ type: 'test', payload: input })
  })
})
describe('createAction("test", (a, b) => ({ a, b }))', () => {
  let actionCreator = createAction('test', (a, b) => ({ a, b }))

  it('returns an actionCreator with toString() => "test"', () => {
    expect(actionCreator.toString()).toEqual('test')
  })
  it('actionCreator returns an object with type: "test"', () => {
    expect(actionCreator()).toEqual({ type: 'test' })
  })
  it('actionCreator(input) returns an object with a: input, b: undefined', () => {
    let a = 'test2'
    expect(actionCreator(a)).toEqual({ type: 'test', a, b: undefined })
  })
  it('actionCreator(input1, input2) returns an object with a: input1, b: input2', () => {
    let a = 'test2'
    let b = 'test3'
    expect(actionCreator(a, b)).toEqual({ type: 'test', a, b })
  })
})
