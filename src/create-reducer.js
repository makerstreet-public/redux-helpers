import { ACTION_TYPE_DELIMITER } from './constants'

export default (domain, handlers, defaultState) => {
  handlers = handlers || []
  let reducers = {}
  Object.entries(handlers).forEach(([handler, reducer]) => {
    let handlers = String(handler).split(ACTION_TYPE_DELIMITER)
    handlers.forEach(handler => {
      let arr = reducers[handler] = reducers[handler] || []
      arr.push(reducer)
    })
  })
  let reducer = (state, action) => {
    if (state === undefined) {
      state = defaultState
    }
    if (!action || action.type === undefined) {
      return state
    }
    let _reducers = reducers[action.type]
    if (!_reducers || !_reducers.length) {
      return state
    }
    return _reducers.reduce((state, reducer) => reducer(state, action), state)
  }
  reducer.toString = () => domain
  return reducer
}
