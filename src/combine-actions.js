import { ACTION_TYPE_DELIMITER } from './constants'

export default (...actions) => {
  const combinedType = actions.map(action => action.toString()).join(ACTION_TYPE_DELIMITER)
  return String(combinedType)
}
