export const metaKey = process.env.NODE_ENV === 'production'
  ? Symbol('touchtribe/resolvable-action')
  : 'touchtribe/resolvable-action'

export const getPromise = (action) => action[metaKey]

export function withPromise (action, promise) {
  action[metaKey] = promise
  return action
}
