export default (type, identityReducer = (payload) => ({ payload })) => {
  let action = (...params) => ({ type, ...identityReducer(...params) })
  action.toString = () => type.toString()
  return action
}
