import combineActions from './combine-actions'
import { ACTION_TYPE_DELIMITER } from './constants'

describe('combineActions', () => {
  it('it returns a single type when combining a single action', () => {
    expect(combineActions('TEST')).toBe('TEST')
  })
  it('it returns two types seperated by a delimiter', () => {
    expect(combineActions('TEST1', 'TEST2')).toBe(`TEST1${ACTION_TYPE_DELIMITER}TEST2`)
  })
  it('it returns a string', () => {
    expect(typeof combineActions('TEST')).toBe('string')
  })
})
